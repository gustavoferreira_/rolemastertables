
import sys
import os
import subprocess
from subprocess import call

from time import sleep

from PyQt5.QtCore import QProcess, Qt, QTimer
from PyQt5.QtGui import QColor, QCursor, QIcon
from PyQt5.QtWidgets import (QApplication, QDial, QGraphicsDropShadowEffect,
                             QGroupBox, QLabel, QMainWindow, QMenu, QPushButton,
                             QShortcut, QSystemTrayIcon, QVBoxLayout)




class MainWindow(QMainWindow):

    """Voice Changer main window."""
    def __init__(self, parent=None):
        super(MainWindow, self).__init__()
        QMainWindow.__init__(self, None, Qt.WindowStaysOnTopHint)


        #comandos para mudar o som
        self.comando1 = "pacmd move-source-output %s virtmic"
        self.comando2 = "ffmpeg -re -i %s %s -f  s16le -ar 16000 -ac 1 - > /home/gustavo/audio/virtmic"
        #self.comando2 = "play %s > /home/gustavo/audio/virtmic"
        self.comando3 = "pacmd move-source-output %s alsa_input.usb-Kingston_HyperX_Virtual_Surround_Sound_00000000-00.analog-stereo"
        # pega o index
        #os.system("pacmd list-source-outputs")
        variavel = str(subprocess.check_output(['pacmd', 'list-source-outputs']))
        #print ('variavel', variavel)
        posini = variavel.find('index')
        posfim = variavel[posini:].find('\\n')
        resposta = variavel[posini:posfim+posini]
        resposta = resposta.replace('index: ','')
        print('resposta', resposta)
        self.index = 571    #str(resposta)
        self.comando1 = self.comando1 % self.index
        self.comando3 = self.comando3 % self.index
        #
        self.statusBar().showMessage("Escolha a musica.")
        self.setWindowTitle(__doc__)
        self.setMinimumSize(200, 1000)
        #self.setMaximumSize(100, 480)
        self.resize(self.minimumSize())
        self.setWindowIcon(QIcon.fromTheme("audio-input-microphone"))
        self.tray = QSystemTrayIcon(self)
        self.move(4000,0)
        QShortcut("Ctrl+q", self, activated=lambda: self.close())
        self.menuBar().addMenu("&File").addAction("Quit", lambda: exit())
        #self.menuBar().addMenu("Sound").addAction(
       #     "STOP !", lambda: call('killall rec', shell=True))
        windowMenu = self.menuBar().addMenu("&Window")
        windowMenu.addAction("Hide", lambda: self.hide())
        windowMenu.addAction("Minimize", lambda: self.showMinimized())
        windowMenu.addAction("Maximize", lambda: self.showMaximized())
        windowMenu.addAction("Restore", lambda: self.showNormal())
        windowMenu.addAction("FullScreen", lambda: self.showFullScreen())
        windowMenu.addAction("Center", lambda: self.center())
        windowMenu.addAction("Top-Left", lambda: self.move(0, 0))
        windowMenu.addAction("To Mouse", lambda: self.move_to_mouse_position())
        # widgets
        group0 = QGroupBox("Escolha a voz")
        self.setCentralWidget(group0)
        self.process = QProcess(self)
        #self.process.error.connect(
        #    lambda: self.statusBar().showMessage("Info: Process Killed", 5000))
        self.control = QDial()
        self.control.setRange(-10, 20)
        self.control.setSingleStep(5)
        self.control.setValue(0)
        self.control.setCursor(QCursor(Qt.OpenHandCursor))
        #self.control.sliderPressed.connect(
        #    lambda: self.control.setCursor(QCursor(Qt.ClosedHandCursor)))
        #self.control.sliderReleased.connect(
        #    lambda: self.control.setCursor(QCursor(Qt.OpenHandCursor)))
        #self.control.valueChanged.connect(
        #    lambda: self.control.setToolTip(f"<b>{self.control.value()}"))
        #self.control.valueChanged.connect(
        #    lambda: self.statusBar().showMessage(
        #        f"Voice deformation: {self.control.value()}", 5000))
        #self.control.valueChanged.connect(self.run)
        #self.control.valueChanged.connect(lambda: self.process.kill())
        # Graphic effect
        """
        self.glow = QGraphicsDropShadowEffect(self)
        self.glow.setOffset(0)
        self.glow.setBlurRadius(99)
        self.glow.setColor(QColor(99, 255, 255))
        self.control.setGraphicsEffect(self.glow)
        self.glow.setEnabled(False)
        """
        # Timer to start
        """
        self.slider_timer = QTimer(self)
        self.slider_timer.setSingleShot(True)
        self.slider_timer.timeout.connect(self.on_slider_timer_timeout)
        """
        # an icon and set focus
        QLabel(self.control).setPixmap(
            QIcon.fromTheme("audio-input-microphone").pixmap(32))
        self.control.setFocus()

        b1=QPushButton("Monstro rugindo")
        b2=QPushButton("Mostra cheirando")
        b3=QPushButton("Monstro morrendo")
        b4=QPushButton("Zombie")
        b5=QPushButton("Rainha morrendo")
        b6=QPushButton("Monstro Matando")

        b20=QPushButton("FireBolt")
        b21=QPushButton("FireBall")
        b22=QPushButton("WaterBall")
        b23=QPushButton("FireWall")
        b24=QPushButton("elfico 1")
        b25=QPushButton("elfico 2")
        b26=QPushButton("elfico 3")
        b27=QPushButton("elfico m")
        b28=QPushButton("magia + estalo")

        b30=QPushButton("Risada feminina")
        b31=QPushButton("Risada masculina")
        b32=QPushButton("crianca gritando")
        b33=QPushButton("Bocejo masculino")
        b34=QPushButton("Bocejo feminino")

        b40=QPushButton("Homem morrendo")
        b41=QPushButton("Homem Ferrido")
        b42=QPushButton("Ferimento 1")
        b43=QPushButton("Ferimento 2")
        b44=QPushButton("Ferimento 3")


        b50=QPushButton("Batendo Madeira")
        b51=QPushButton("Porta abrindo")
        b52=QPushButton("Engrenagem")


        b60=QPushButton("Soco")
        b61=QPushButton("Machado")
        b62=QPushButton("Flecha")
        b63=QPushButton("Chicote")
        b64=QPushButton("Torcha")
        b65=QPushButton("rio")
        b66=QPushButton("Pulandonaagua")
        b67=QPushButton("Tempestade")
        b68=QPushButton("Espada")
        b69=QPushButton("Espada 2")
        b70=QPushButton("Espada Defendendo")
        #b63=QPushButton("")

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(b1)
        mainLayout.addWidget(b2)
        mainLayout.addWidget(b3)
        mainLayout.addWidget(b4)
        mainLayout.addWidget(b5)
        mainLayout.addWidget(b6)

        mainLayout.addWidget(b20)
        mainLayout.addWidget(b21)
        mainLayout.addWidget(b22)
        mainLayout.addWidget(b23)
        mainLayout.addWidget(b24)
        mainLayout.addWidget(b25)
        mainLayout.addWidget(b26)
        mainLayout.addWidget(b27)
        mainLayout.addWidget(b28)

        mainLayout.addWidget(b30)
        mainLayout.addWidget(b31)
        mainLayout.addWidget(b32)
        mainLayout.addWidget(b33)
        mainLayout.addWidget(b34)

        mainLayout.addWidget(b40)
        mainLayout.addWidget(b41)
        mainLayout.addWidget(b42)
        mainLayout.addWidget(b43)
        mainLayout.addWidget(b44)

        mainLayout.addWidget(b50)
        mainLayout.addWidget(b51)
        mainLayout.addWidget(b52)

        mainLayout.addWidget(b60)
        mainLayout.addWidget(b61)
        mainLayout.addWidget(b62)
        mainLayout.addWidget(b63)
        mainLayout.addWidget(b64)
        mainLayout.addWidget(b65)
        mainLayout.addWidget(b66)
        mainLayout.addWidget(b67)
        mainLayout.addWidget(b68)
        mainLayout.addWidget(b69)
        mainLayout.addWidget(b70)


        b1.clicked.connect(self.b1_click)
        b2.clicked.connect(self.b2_click)
        b3.clicked.connect(self.b3_click)
        b4.clicked.connect(self.b4_click)
        b5.clicked.connect(self.b5_click)
        b6.clicked.connect(self.b6_click)


        b20.clicked.connect(self.b20_click)
        b21.clicked.connect(self.b21_click)
        b22.clicked.connect(self.b22_click)
        b23.clicked.connect(self.b23_click)
        b24.clicked.connect(self.b24_click)
        b25.clicked.connect(self.b25_click)
        b26.clicked.connect(self.b26_click)
        b27.clicked.connect(self.b27_click)
        b28.clicked.connect(self.b28_click)


        b30.clicked.connect(self.b30_click)
        b31.clicked.connect(self.b31_click)
        b32.clicked.connect(self.b32_click)
        b33.clicked.connect(self.b33_click)
        b34.clicked.connect(self.b34_click)

        b40.clicked.connect(self.b40_click)
        b41.clicked.connect(self.b41_click)
        b42.clicked.connect(self.b42_click)
        b43.clicked.connect(self.b43_click)
        b44.clicked.connect(self.b44_click)

        b50.clicked.connect(self.b50_click)
        b51.clicked.connect(self.b51_click)
        b52.clicked.connect(self.b52_click)

        b60.clicked.connect(self.b60_click)
        b61.clicked.connect(self.b61_click)
        b62.clicked.connect(self.b62_click)
        b63.clicked.connect(self.b63_click)
        b64.clicked.connect(self.b64_click)
        b65.clicked.connect(self.b65_click)
        b66.clicked.connect(self.b66_click)
        b67.clicked.connect(self.b67_click)
        b68.clicked.connect(self.b68_click)
        b69.clicked.connect(self.b69_click)
        b70.clicked.connect(self.b70_click)

        #self.setLayout(mainLayout)
        group0.setLayout(mainLayout)
        #self.setCentralWidget(mainLayout)
        #QVBoxLayout(group0).addWidget(self.control)
        self.menu = QMenu(__doc__)
        #self.menu.addAction(__doc__).setDisabled(True)
        self.menu.setIcon(self.windowIcon())
        self.menu.addSeparator()
        """
        self.menu.addAction(
            "Show / Hide",
            lambda: self.hide() if self.isVisible() else self.showNormal())
        self.menu.addAction("STOP !", lambda: call('killall rec', shell=True))
        self.menu.addSeparator()
        self.menu.addAction("Quit", lambda: exit())
        """
        self.tray.setContextMenu(self.menu)
        self.make_trayicon()

    def b1_click(self):
        volume = '-af volume=-0.1'
        #musica  = self.comando2 % 'Dragon_Growl_00.wav'
        musica  = self.comando2 % ('Monster_03.wav', volume) # monstro rungindo
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b2_click(self):
        volume = '-af volume=-0.5'
        musica  = self.comando2 % ('Monster_05.wav', volume) # monstro cheirando
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b3_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('monstromorrendo.wav', volume) # monstro morrendo Monster_02.wav
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b4_click(self):
        volume = ''
        musica  = self.comando2 % ('brains3.wav', volume) # Zombie_03.wav zombie
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b5_click(self):
        volume = '-af volume=-0.1'
        musica  = self.comando2 % ('scream2.wav', volume) # rainha aranha morrendo
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b6_click(self):
        volume = '-af volume=-0.2'
        musica  = self.comando2 % ('monstromatando.wav', volume) # rainha aranha morrendo
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)



    def b20_click(self):
        volume = ''
        musica  = self.comando2 % ('firebolt.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b21_click(self):
        volume = ''
        musica  = self.comando2 % ('FireImpact.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b22_click(self):
        volume = ''
        musica  = self.comando2 % ('waterball.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b23_click(self):
        volume = ''
        musica  = self.comando2 % ('firewall.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b23_click(self):
        volume = ''
        musica  = self.comando2 % ('firewall.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b24_click(self):
        volume = ''
        musica  = self.comando2 % ('elfico1.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b25_click(self):
        volume = ''
        musica  = self.comando2 % ('elfico2.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b26_click(self):
        volume = ''
        musica  = self.comando2 % ('elfico3.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b27_click(self):
        volume = ''
        musica  = self.comando2 % ('elficom.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b28_click(self):
        volume = ''
        musica  = self.comando2 % ('magiaestalo.wav', volume)
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)



    def b30_click(self):
        volume = ''
        musica  = self.comando2 % ('witch_cackle.wav', volume) # risada feminina
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b31_click(self):
        volume = ''
        musica  = self.comando2 % ('Laugh_Evil_00.wav', volume) #risada masculina
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b32_click(self):
        volume = '-af volume=-0.2'
        musica  = self.comando2 % ('scream3.wav', volume) # crianca mulher gritando
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b33_click(self):
        volume = '-af volume=-0.2'
        musica  = self.comando2 % ('bocejom.wav', volume) # crianca mulher gritando
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b34_click(self):
        volume = ''
        musica  = self.comando2 % ('bocejof.wav', volume) # crianca mulher gritando
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)



    def b40_click(self):
        volume = '-af volume=-0.2'
        musica  = self.comando2 % ('Scream_Male_00.wav', volume) # homem  morrendo
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b41_click(self):
        volume = '-af volume=-0.2'
        musica  = self.comando2 % ('Scream_Male_02.wav', volume) # homem feriado
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b42_click(self):
        volume = '-af volume=-0.2'
        musica  = self.comando2 % ('ferida.wav', volume) # homem feriado
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)        

    def b43_click(self):
        volume = ''
        musica  = self.comando2 % ('ferida2.wav', volume) # homem feriado
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)     

    def b44_click(self):
        volume = ''
        musica  = self.comando2 % ('ferida3.wav', volume) # homem feriado
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)             


    def b50_click(self):
        volume = ''
        musica  = self.comando2 % ('batendomadeira.wav', volume) #batendo madeira
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b51_click(self):
        volume = ''
        musica  = self.comando2 % ('portaabrindo.wav', volume) # porta
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b52_click(self):
        volume = ''
        musica  = self.comando2 % ('engrenagem.wav', volume) # engrenagem
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b60_click(self):
        volume = '-af volume=-0.5'
        musica  = self.comando2 % ('soco.wav', volume) # soco
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b61_click(self):
        volume = '-af volume=-0.5'
        musica  = self.comando2 % ('machado.wav', volume) # machado
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b62_click(self):
        volume = ''
        musica  = self.comando2 % ('flecha.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b63_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('chicote.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b64_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('torch.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b65_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('rio.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b66_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('pulandonaagua.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b67_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('tempestade.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b68_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('espada.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b69_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('espada2.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def b70_click(self):
        volume = '-af volume=-0.3'
        musica  = self.comando2 % ('espadadefende.wav', volume) # flecha
        comando = '%s && %s && %s' % (self.comando1, musica, self.comando3)
        print (comando)
        os.system(comando)

    def Cancel(self):
        self.close()

    def closeEvent(self):
        self.deleteLater()


    def run(self):
        """Run/Stop the QTimer."""
        if self.slider_timer.isActive():
            self.slider_timer.stop()
        self.glow.setEnabled(True)
        call('killall rec ; killall play', shell=True)
        self.slider_timer.start(3000)

    def on_slider_timer_timeout(self):
        """Run subprocess to deform voice."""
        self.glow.setEnabled(False)
        value = int(self.control.value()) * 100
        command = f'play -q -V0 "|rec -q -V0 -n -d -R riaa bend pitch {value} "'
        print(f"Voice Deformation Value: {value}")
        print(f"Voice Deformation Command: {command}")
        self.process.start(command)
        if self.isVisible():
            self.statusBar().showMessage("Minimizing to System TrayIcon", 3000)
            print("Minimizing Main Window to System TrayIcon now...")
            sleep(3)
            self.hide()

    def center(self):
        """Center Window on the Current Screen,with Multi-Monitor support."""
        window_geometry = self.frameGeometry()
        mousepointer_position = QApplication.desktop().cursor().pos()
        screen = QApplication.desktop().screenNumber(mousepointer_position)
        centerPoint = QApplication.desktop().screenGeometry(screen).center()
        window_geometry.moveCenter(centerPoint)
        self.move(window_geometry.topLeft())

    def move_to_mouse_position(self):
        """Center the Window on the Current Mouse position."""
        window_geometry = self.frameGeometry()
        window_geometry.moveCenter(QApplication.desktop().cursor().pos())
        self.move(window_geometry.topLeft())

    def make_trayicon(self):
        """Make a Tray Icon."""
        if self.windowIcon() and __doc__:
            self.tray.setIcon(self.windowIcon())
            self.tray.setToolTip(__doc__)
            self.tray.activated.connect(
                lambda: self.hide() if self.isVisible()
                else self.showNormal())
            return self.tray.show()


###############################################################################


def main():
    """Main Loop."""
    application = QApplication(sys.argv)
    application.setApplicationName("Menu Voz")
    application.setOrganizationName("Gustavo")
    application.setOrganizationDomain("Gustavo")
    application.setWindowIcon(QIcon.fromTheme("audio-input-microphone"))
    #application.aboutToQuit.connect(
    #   lambda: call('killall rec ; killall play', shell=True))
    mainwindow = MainWindow()
    mainwindow.show()
    sys.exit(application.exec_())


if __name__ in '__main__':
    main()
